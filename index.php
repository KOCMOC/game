<html>
	<head>
		<link rel="stylesheet" type="text/css" href="./styles.css"></style>
	    <script type="text/javascript" src="./jquery-3.0.0-alpha1.js"></script>
	    <script type="text/javascript" src="./script.js"></script>
	    <script type="text/javascript" src="./astar.js"></script>
	</head>
	<body>
		<div class="menu">Включить прозрачность&nbsp;<input type="checkbox" onchange="$('#figures').toggleClass('opacity')"></div>
		<svg width="1000" height="500" viewBox="0 0 1000 500" style="border:1px solid #000;margin-top:20px/*временное*/">
		<!--<svg width="1500" height="700" viewBox="0 0 1500 700" style="border:1px solid #000;margin-top:20px/*временное*/">-->
			<?php
				$is = 500;
				$js = 1000;
				//$is = 700;
				//$js = 1500;
				$zeroPoints = array(
					array('20', '30'),
					array('0', '15'),
					array('0', '15'),
					array('20', '0'),
					array('40', '0'),
					array('60', '15'),
					array('40', '30')
					/*array('20', '20'),
					array('0', '10'),
					array('0', '10'),
					array('20', '0'),
					array('40', '0'),
					array('60', '10'),
					array('40', '20')*/
					/*array('10', '40'),
					array('0', '20'),
					array('0', '20'),
					array('10', '0'),
					array('30', '0'),
					array('40', '20'),
					array('30', '40')*/
				);
			?>
			<?
				$ic = 0;
				$jc = 0;
				$offset = true;
				for($i=0; $i<$is-10; $i+=30){
				//for($i=0; $i<$is-10; $i+=20){
				//for($i=0; $i<$is-20; $i+=40) {
					$ic++;
					$jc = 0;
					$top = false;
					for($j=0; $j<$js-40; $j+=40){
					//for($j=0; $j<$js-40; $j+=40){
					//for($j=0; $j<$js-30; $j+=30){
						$top = !$top;
						$offsetTop = ($top)?0:15;
						//$offsetTop = ($top)?0:10;
						//$offsetTop = ($top)?0:20;
						$jc++;
						$points = "";
						foreach($zeroPoints as $coords) {
							$points .= ($coords[0]+$j).",".($coords[1]+$i+$offsetTop)." ";
						}
						?>
						<polygon class="field" id="p<?=$ic.'_'.$jc ?>" points="<?=trim($points) ?>" x="<?= $ic?>" y="<?=$jc ?>" />
						<?
					}
				}
				?>
			?>
			<?/*<polygon id="p1" points="10, 40 0, 20 0, 20 10, 0 30, 0 40, 20 30, 40" fill="rgb(240,240,240)" style="fill-opacity: 1" stroke-width="0.15" stroke="rgb(0,0,0)" />
		*/?></svg>
		<div id="figures">
			<div id="f1" x="1" y="1" radius="3" belong="me">
				<img src="chuvak.png">
			</div>
			<div id="f2" x="11" y="7" radius="15" belong="me">
				<img src="chuvak.png">
			</div>

			<div id="f3" x="4" y="10" radius="2" belong="enemy">
				<img src="chuvak.png">
			</div>
			<div id="f4" x="6" y="10" radius="2" belong="enemy">
				<img src="chuvak.png">
			</div>
			<div id="f5" x="8" y="10" radius="2" belong="enemy">
				<img src="chuvak.png">
			</div>
			<div id="f6" x="10" y="10" radius="2" belong="enemy">
				<img src="chuvak.png">
			</div>
			<div id="f7" x="12" y="10" radius="2" belong="enemy">
				<img src="chuvak.png">
			</div>
			<div id="f8" x="14" y="10" radius="2" belong="enemy">
				<img src="chuvak.png">
			</div>
			<div id="f9" x="16" y="10" radius="2" belong="enemy">
				<img src="chuvak.png">
			</div>
		</div>
		<script type="text/javascript">
			var horOffset = 4;
			//var horOffset = 0;
			var vertOffset = 5;
			//var vertOffset = 25;
			var fuckups = ['8_20', '9_20', '10_20', '11_20', '12_20', '13_20', '14_20'];
		</script>
	</body>
</html>