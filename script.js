var fields =[], active = false, radiusFields = [], delay = 0;
$(document).ready(function(){
	$('#figures > div').each(function(){
		// предварительная расстановка, не забыть про fuckups
    	fields[$(this).attr('id')] = {where: 'p'+$(this).attr('x')+'_'+$(this).attr('y'), pos: {x:$(this).attr('x'), y:$(this).attr('y')}, rad: $(this).attr('radius'), bel: $(this).attr('belong')};
    	if($(this).attr('belong') == 'enemy') {    		$('#'+'p'+$(this).attr('x')+'_'+$(this).attr('y')).addClass('enemy').attr('enemy', $(this).attr('id'));    	}
    	$(this).css({top:$('#p'+$(this).attr('x')+'_'+$(this).attr('y')).position().top - $(this).height() + vertOffset, left:$('#p'+$(this).attr('x')+'_'+$(this).attr('y')).position().left + horOffset});
    	$(this).removeAttr('belong').removeAttr('radius').removeAttr('x').removeAttr('y');
	});
	for(var fuckup in fuckups) {
	    $('#p'+fuckups[fuckup]).addClass('fuckup');
	}
	$('.field').click(function(){
		if(active !== false && !$(this).hasClass('fuckup')) {
    		if($(this).hasClass('enemy')) {                attackFigure($(this).attr('enemy'));    		} else {
    			moveFigure($(this).attr('id'));
    		}
    	}
	});
	$('#figures > div').click(function(){
		if(fields[$(this).attr('id')].bel == 'me' && active != $(this).attr('id')) {
		    if(active !== false) {
		    	$('#'+active).removeClass('active');
		    	$('#'+fields[active].where).removeClass('active');
		    	$('.field.zone').removeClass('zone');
		    }
		    $(this).addClass('active');
		    active = $(this).attr('id');
		    $('#'+fields[active].where).addClass('active');
		    getRadiusFields(active);
	    } else if(active !== false && fields[$(this).attr('id')].bel == 'enemy') {
	        attackFigure($(this).attr('id'));
	    }
	});
});

function moveFigure(position, nullDelay = true) {
	var field = $('#'+position), path = [];
	if(field.hasClass('zone')) {
		$('#'+fields[active].where).removeClass('active');
		path = calcPath({x:field.attr('x'), y:field.attr('y')});
		$('.field.zone').removeClass('zone');
		delay = 300 * path.length;
		setTimeout(function(){if(nullDelay === true) {$('#'+active).removeClass('active');active = false;delay = 0}}, delay);
		for(var i in path) {
			$('#'+active).animate({
				top:$('#'+path[i]).position().top - $('#'+active).height() + vertOffset,
				left:$('#'+path[i]).position().left + horOffset
			}, {
			    duration: 300,
			    easing: "linear"
			});
		}
		fields[active].where = field.attr('id');
		fields[active].pos.x = field.attr('x');
		fields[active].pos.y = field.attr('y');
	} else {
	    alert('Далековато...');
	}
}
function attackFigure(idEnemy) {
	var idActive = active;
    // отправляем idActive и idEnemy
	/* отправка запроса на сервер. Если ответ положительный (стрелковое на игроке или в зоне досягаемости),
	   то 2 команды коллбека, либо пуск из точки в точку (во врага или в препятствие),
	   либо атака врукопашную - подойти на максимально близкое расстояние и нанести удар */
	//releaseAttackCallback();
	//meleeAttackCallback({x:11, y:9}, 'f2', 'f5');
	serverResponseImitation(idActive, idEnemy);
}
/* Тестовая */
function serverResponseImitation(idActive, idEnemy) {	var path = calcPath(fields[idEnemy].pos, true);
	if(path.length > 1) {
		path.reverse();
		path.splice(0, 1);
		var pos = path[0].substr(1).split('_');
		meleeAttackCallback({x:pos[0], y:pos[1]}, idActive, idEnemy);
	} else {	    meleeAttackCallback(fields[idActive].pos, idActive, idEnemy);	}}
/* КонецТестовая */

function meleeAttackCallback(atkPos, atkId, enemyId) {    if(fields[atkId].pos.x != atkPos.x || fields[atkId].pos.y != atkPos.y) {    	moveFigure('p'+atkPos.x+'_'+atkPos.y, false);
        setTimeout(function() {meleeAttackAnimation(atkId, enemyId);delay=0;}, delay);
    } else {        meleeAttackAnimation(atkId, enemyId);    }}

function meleeAttackAnimation(atkId, enemyId) {
   	$('#'+fields[active].where).removeClass('active');
   	$('.field.zone').removeClass('zone');    $('#'+atkId).addClass('attack');
    $('#'+enemyId).addClass('attacked').addClass('active');
	setTimeout(function(){$('#'+atkId).removeClass('attack');$('#'+enemyId).removeClass('attacked').removeClass('active');$('#'+active).removeClass('active');active = false;}, 1000);}

function dec_to_cube(a, b) {
    x = b;
    z = a - (b - (b&1)) / 2
    y = -x-z;
    return {x:x, y:y, z:z};
}

function heuristic(from, to) {
    return Math.abs(from.x - to.x) + Math.abs(from.y - to.y);
}

function findRadius(radius) {
	var results = [], maxrad, minrad;
    for (var i=-radius; i<=radius; i++) {
		maxrad = Math.max(-radius, -i-radius);
		minrad = Math.min(radius, -i+parseInt(radius));
	    for(var j=maxrad; j<=minrad; j++) {
	        if(i!=0 || j!=0) {
				results.push({x:i, y:j, z:-i-j});
			}
	    }
	}
	return results;
}

function findNeight(pos) {
    var results = [], dx, dy, radiuses = findRadius(1);
    for(var res in radiuses) {
		if(pos.y&1) {
	    	dx = radiuses[res].x + parseInt(pos.y);
	    	dy = radiuses[res].z + (radiuses[res].x - (radiuses[res].x&1)) / 2 + parseInt(pos.x);
    	} else {
			dx = radiuses[res].x + parseInt(pos.y);
	    	dy = radiuses[res].z + (radiuses[res].x + (radiuses[res].x&1)) / 2 + parseInt(pos.x);
    	}
        results.push({where:'p'+dy+'_'+dx, pos:{x:dy, y:dx}});
    }
    return results;
}

function getRadiusFields(position) {
    var a = fields[position].pos.x, b = fields[position].pos.y, radius = fields[position].rad,
    	dx, dy, results = [];

    results = findRadius(radius);

	for(var i in results) {
		if(b&1) {
	    	dx = results[i].x + parseInt(b);
	    	dy = results[i].z + (results[i].x - (results[i].x&1)) / 2 + parseInt(a);
    	} else {
			dx = results[i].x + parseInt(b);
	    	dy = results[i].z + (results[i].x + (results[i].x&1)) / 2 + parseInt(a);
    	}

        $('#p'+dy+'_'+dx).addClass('zone');
	}
}

function calcPath(pos, we = false) {
	var neights = [], new_cost, graph_cost, priority, from, path = [];
	var start = fields[active].pos;
    //var end = {x:x, y:y};
    var goal = {where:'p'+pos.x+'_'+pos.y, pos:{x:pos.x, y:pos.y}};
    var queue = new Queue();
    var came_from = {};
    var cost_so_far = {};
    came_from[fields[active].where] = false;
    cost_so_far[fields[active].where] = 0;

    queue.put(fields[active], 0);

    while(queue.length > 0) {
    	current = queue.get();

    	if(current.where == goal.where) {
            break;
    	}

    	neights = findNeight(current.pos);
    	for(next in neights) if((we || !$('#'+neights[next].where).hasClass('enemy')) && !$('#'+neights[next].where).hasClass('fuckup') && $('#'+neights[next].where).hasClass('zone')) {
        	new_cost = cost_so_far[current.where] + 1;
        	if (!cost_so_far[neights[next].where] || new_cost < cost_so_far[neights[next].where]) {
				cost_so_far[neights[next].where] = new_cost;
				priority = new_cost + heuristic(goal.pos, neights[next].pos);
				queue.put(neights[next], priority);
				came_from[neights[next].where] = current;
        	}
    	}
    }

	var from = goal.where;
	while(from != fields[active].where) {	    path.push(from);
	    from = came_from[from].where;	}
	path.reverse();
	return path;
}

function Queue() {
    this.queue = {};
    this.length = 0;
}

Queue.prototype.put = function(next, priority) {
	if(!this.queue[priority]) {
	    this.queue[priority] = [];
	}
	this.queue[priority].push(next);
	this.length++;
}
Queue.prototype.get = function() {
	if(this.length === 0) return false;
	var first = false, ret = false;
	for(var i in this.queue) {
        first = i;
	    break;
	}
	if(first === false) {
	    return false;
	}
    ret = this.queue[first][0];
    this.queue[first].splice(0, 1);
    if(this.queue[first].length === 0) {
    	delete this.queue[first];
    }
    this.length--;
    return ret;
}

/*

frontier = PriorityQueue()
frontier.put(start, 0)
came_from = {}
cost_so_far = {}
came_from[start] = None
cost_so_far[start] = 0

while not frontier.empty():
   current = frontier.get()

   if current == goal:
      break

   for next in graph.neighbors(current):
      new_cost = cost_so_far[current] + graph.cost(current, next)
      if next not in cost_so_far or new_cost < cost_so_far[next]:
         cost_so_far[next] = new_cost
         priority = new_cost
         frontier.put(next, priority)
         came_from[next] = current

*/

